#Tumble-Like Tag Overflow#

A neat way of showing lots of tags on a post without compromising the design with blocks of tags. Inspired by Tumblr's overflow solution.

You will need the following HTML markup and CSS style for the script to work;


```
#!HTML

<article class='post'>
  <h1>Tumblr-like tag overflow</h1>
  <p>Click and drag the overflowing tags to see more. Inspired by Tubmlr and how they deal with an excess of tags.</p>
  <hr>
  <div class='post_tags draggable'>
    <div class='post_tags_inner'>
      <a href='#'>venenatis</a>
      <a href='#'>vitae</a>
      <a href='#'>ultricies</a>
      <a href='#'>phasellus</a>
      <a href='#'>accumsan</a>
      <a href='#'>morbi</a>
      <a href='#'>consectetur</a>
      <a href='#'>pellentesque</a>
    </div>
  </div>
</article>
```


```
#!CSS

.post_tags {
  position: relative;
  margin-top: 10px;
  line-height: 20px;
  white-space: nowrap;
  overflow: hidden;
}
html.touch .post_tags {
  /* If touch device, the jQuery script will be disabled and a scrollbar will be put in its place to allow mobile scrolling */
  overflow-x: scroll !important;
}
.post_tags.draggable .post_tags_inner {
  cursor: col-resize;
  -webkit-touch-callout: none;
  -webkit-user-select: none;
  -khtml-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}
.post_tags_inner {
  float: left;
  position: relative;
  padding: 0px 20px 0px 0;
}
.post_tags a,
.post_tags .post_tag {
  color: #A7A7A7;
  font-size: 15px;
  text-decoration: none;
  margin-right: 6px;
  text-transform: lowercase !important;
}
.post_tags a:before {
  content: '#';
}
.post_tags:after {
  content: '';
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  width: 20px;
  background: -moz-linear-gradient(left, rgba(255, 255, 255, 0) 0, rgba(255, 255, 255, 1) 100%);
  background: -webkit-gradient(linear, left top, right top, color-stop(0%, rgba(255, 255, 255, 0)), color-stop(100%, #FFF));
  background: -webkit-linear-gradient(left, rgba(255, 255, 255, 0) 0, #FFF 100%);
  background: -o-linear-gradient(left, rgba(255, 255, 255, 0) 0, rgba(255, 255, 255, 1) 100%);
  background: -ms-linear-gradient(left, rgba(255, 255, 255, 0) 0, rgba(255, 255, 255, 1) 100%);
  background: linear-gradient(to right, rgba(255, 255, 255, 0) 0, #FFF 100%);
}
html.touch .post_tags:after {content:none; /* Remove gradient if mobile, doesn't stay fixed to the right hand side */}
.post_tags a:hover,
.post_tags .post_tag:hover,
.post_tags a:focus,
.post_tags .post_tag:focus,
.post_tags a:active,
.post_tags .post_tag:active {
  color: #54B2D3;
}
```

You will also require the following Javascript Libraries;


```
#!javascript

<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
```

and finally, the Script snippet;


```
#!jQuery

$(document).ready(function() {
	if($("html").hasClass("no-touch")){ // If modernizr has detected no touch support
	    $(".post_tags_inner").draggable({
	      	axis: "x",
		  	scroll: false,
		  	stop: function() {
		        var __left = $(this).css("left").replace(/[^-\d\.]/g, '');
		        if (__left > 0) {
		        	$(this).animate({
		            	left: 0
		        	}, 400, 'easeOutExpo');
		        }
		        var __width = $(this).outerWidth();
		        var __parentWidth = $(".post_tags.draggable").outerWidth();
		        if (__width > __parentWidth) {
		        	if (__left < __parentWidth - __width) { //If scrolled too-far right (positive +)
		            	$(this).animate({
		            		left: __parentWidth - __width
		            	}, 400, 'easeOutExpo');
		        	} 
		        } else { //Else if scrolled too-far left (negative -)
		        	$(this).animate({
		            	left: 0
		        	}, 400, 'easeOutExpo');
		        }
	      	}
	    });
	} 
});
```